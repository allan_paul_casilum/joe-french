<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
class JF_CustomQueryVar{

  private $response;

  /**
   * instance of this class
   *
   * @since 3.12
   * @access protected
   * @var null
   * */
  protected static $instance = null;
  protected $slug = 's-realty';

  /**
   * use for magic setters and getter
   * we can use this when we instantiate the class
   * it holds the variable from __set
   *
   * @see function __get, function __set
   * @access protected
   * @var array
   * */
  protected $vars = array();
  /**
   * Return an instance of this class.
   *
   * @since     1.0.0
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() {

    /*
     * @TODO :
     *
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }

  public function __construct()
  {

  }

  public function init()
  {
    add_action('init', array($this, 'jf_rewrite_tag_rule'), 10, 0);
    add_filter('query_vars', array($this, 'jf_register_query_vars') );
    add_filter('template_include', array($this, 'jf_property_template'), 99 );
    add_filter( 'wp_title', array($this, 'wpTitle'), 10);
  }

  public function getSlug()
  {
    return $this->slug;
  }
  /**
   * Add rewrite tags and rules
   *
   * @link https://codex.wordpress.org/Rewrite_API/add_rewrite_tag
   * @link https://codex.wordpress.org/Rewrite_API/add_rewrite_rule
   */
  /**
   * Add rewrite tags and rules
   */
  public function jf_rewrite_tag_rule()
  {
    global $wp_rewrite;
    $slug = $this->getSlug();
    add_rewrite_tag('%'.$slug.'%', '([^&]+)' );
	  add_rewrite_rule('^'.$slug.'/([^/]*)/?', 'index.php?'.$slug.'=$matches[1]','top' );
    add_rewrite_endpoint($slug, EP_PERMALINK | EP_PAGES );

    //$wp_rewrite->flush_rules();
  }

  /**
   * Register custom query vars
   *
   * @param array $vars The array of available query variables
   *
   * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/query_vars
   */
  public function jf_register_query_vars( $vars ) {
  	$vars[] = $this->getSlug();
  	return $vars;
  }

  public function getPermalink($address, $mls)
  {
    $slug = $this->getSlug();
    return esc_url( home_url( $slug .'/'. sanitize_title($address) .'-'.$mls.'' ) );
  }

  public function jf_property_template($template)
  {
    global $wp_query;
    if(get_query_var($this->getSlug())){
      $template = get_template_part('single-property');
    }
    return $template;
  }

  public function wpTitle($title)
  {
    $title = 'xx';
    return $title;
  }

}
