<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              apyc.com
 * @since             1.0.0
 * @package           Joe_French
 *
 * @wordpress-plugin
 * Plugin Name:       Joe French
 * Plugin URI:        joefrench
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            apyc
 * Author URI:        apyc.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       joe-french
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}
define('PLACESTER_API','9ZSquc3KfNLfkGZkdzhXb1IrQ7MLPR0VZxIKKAKiXiAa');
/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );
/**
 * For autoloading classes
 * */
spl_autoload_register('jf_autoload_class');
function jf_autoload_class($class_name){
    if ( false !== strpos( $class_name, 'JF' ) ) {
        $include_classes_dir = realpath( get_template_directory( __FILE__ ) ) . DIRECTORY_SEPARATOR;
        $admin_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
        $class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        if( file_exists($include_classes_dir . $class_file) ){
            require_once $include_classes_dir . $class_file;
        }
        if( file_exists($admin_classes_dir . $class_file) ){
            require_once $admin_classes_dir . $class_file;
        }
    }
}
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-joe-french-activator.php
 */
function activate_joe_french() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-joe-french-activator.php';
    Joe_French_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-joe-french-deactivator.php
 */
function deactivate_joe_french() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-joe-french-deactivator.php';
    Joe_French_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_joe_french' );
register_deactivation_hook( __FILE__, 'deactivate_joe_french' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-joe-french.php';

require plugin_dir_path( __FILE__ ) . 'function-helpers.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_joe_french() {

    $plugin = new Joe_French();
    $plugin->run();
    $search_filter = array(
        'sort_by' => 'uncur_data',
        'sort_type' => 'desc',
    );
    //JF_Placester::get_instance()->getLocations();
    //JF_Admin_Property::get_instance();
    //JF_Admin_ImportProperty::get_instance();
    JF_WpRealty::get_instance();
    JF_CustomQueryVar::get_instance()->init();
}

add_action('plugins_loaded', 'run_joe_french');
