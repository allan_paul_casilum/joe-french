<div id="jf-wrapper" class="jf-import-property wrap">
    <h1>Download XML Listing for Import</h1>
    <div class="feature-section one-col">
        <p class="lead-description"></p>
    </div>
    <div>
      <h3>Found <?php echo $arr_xml->total;?> Listings</h3>
      <h3><a href="<?php echo $download_xml;?>">Download the XML</a></h3>
    </div>
</div>
