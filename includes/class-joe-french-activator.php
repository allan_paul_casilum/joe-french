<?php

/**
 * Fired during plugin activation
 *
 * @link       apyc.com
 * @since      1.0.0
 *
 * @package    Joe_French
 * @subpackage Joe_French/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Joe_French
 * @subpackage Joe_French/includes
 * @author     apyc <allan.paul.casilum@gmail.com>
 */
class Joe_French_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
