<?php
/**
 * handles request http or ajax
 * */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class JF_Admin_ImportPropertyRequest {
    protected static $instance = null;
    protected $placester;
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() 
    {

        /*
         * @TODO :
         *
         * - Uncomment following lines if the admin class should only be available for super admins
         */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    public function parse()
    {
    }
    
    public function http_post($post)
    {
      dd($post);
    }
    
    public function ajax()
    {
    }
    
    public function __construct()
    {
      $this->placester = new JF_Placester;
    }
}
