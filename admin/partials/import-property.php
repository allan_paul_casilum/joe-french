<div id="jf-wrapper" class="jf-import-property wrap">
    <h1><?php echo $heading;?></h1>
    <div class="feature-section one-col">
        <p class="lead-description"></p>
    </div>
    <form name="filter-posts-form" method="post" action="<?php echo $action;?>">
        <input type="hidden" name="_method" value="<?php echo $method;?>">
        <p><input type="text" name="location_value" class="location_value"></p>
        <p>
          <select name="location_key" class="location_key">
            <option value="address">Address</option>
            <option value="locality">Locality</option>
            <option value="region">Region</option>
            <option value="postal">Postal</option>
            <option value="neighborhood">Neighborhood</option>
            <option value="country">Country</option>
          </select>
        </p>
        <p class="submit"><input type="submit" name="query" id="query" class="button button-primary" value="Search"></p>
    </form>
</div>
