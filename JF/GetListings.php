<?php
/**
 * get property listings, should be with any api, currently we use placesters
 * */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class JF_GetListings {
    protected static $instance = null;
    protected $api;
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() 
    {

        /*
         * @TODO :
         *
         * - Uncomment following lines if the admin class should only be available for super admins
         */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    /**
     * get listings
     * 
     * @param $arg  array | list of arguments used in api's
     * */
    public function listings($arg = array())
    {
      return $this->api->getListings($arg);
    }
    
    public function __construct()
    {
      $this->api = new JF_Placester;
    }
}
