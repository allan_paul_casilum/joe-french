<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
class JF_WpRealtyDB{

  private $response;

  /**
   * instance of this class
   *
   * @since 3.12
   * @access protected
   * @var null
   * */
  protected static $instance = null;

  /**
   * use for magic setters and getter
   * we can use this when we instantiate the class
   * it holds the variable from __set
   *
   * @see function __get, function __set
   * @access protected
   * @var array
   * */
  protected $vars = array();
  protected $table = 'wp_realty_listingsdb';
  protected $wpdb;
  /**
   * Return an instance of this class.
   *
   * @since     1.0.0
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() {

    /*
     * @TODO :
     *
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }

  public function __construct()
  {
    global $wpdb;
    $this->wpdb = $wpdb;
  }

  public function getListingsDB($args = [])
  {
    //jf_dd($args);
    /**
     * Define the array of defaults
     */
    $defaults = array(
    	'limit' => ' LIMIT 10 ',
    	'bedrooms' => '',
    	'bathrooms' => '',
    	'min-price' => '',
    	'max-price' => '',
    	'location' => '',
    	'status' => '',
    	'type' => '',
    	'type' => '',
    	'min-area' => '',
    	'max-area' => '',
    );

    $keyword = '';
    if(isset($args['keyword']) && trim($args['keyword']) != '' ){
      $keyword = ' AND keyword = '.$args['keyword'];
    }

    /*$status = '';
    if(isset($args['status']) && trim($args['status']) != '' ){
      $keyword = ' AND keyword = '.$args['keyword'];
    }*/

    $location = '';
    if(isset($args['location']) && trim($args['location']) != '' ){
      $location = " AND City LIKE '%{$args['location']}%'";
    }

    $type = '';
    if(isset($args['type']) && trim($args['type']) != '' ){
      $type = " AND PropertyType = '{$args['type']}'";
    }

    $bathrooms = '';
    if(isset($args['bathrooms']) && trim($args['bathrooms']) != '' ){
      $bathrooms = " AND BathsTotal <= {$args['bathrooms']}";
    }

    $bedrooms = '';
    if(isset($args['bedrooms']) && trim($args['bedrooms']) != '' ){
      $bedrooms = " AND BedsTotal <= {$args['bedrooms']}";
    }

    $min_price = '';
    if(isset($args['min-price']) && trim($args['min-price']) != '' ){
      $min_price = " AND Price <= {$args['min-price']}";
    }

    $max_price = '';
    if(isset($args['max-price']) && trim($args['max-price']) != '' ){
      $max_price = " AND Price >= {$args['max-price']}";
    }

    $price = '';
    if($min_price != '' && $max_price != '') {
      $price = " AND Price BETWEEN {$args['min-price']} AND {$args['max-price']} ";
    }elseif($min_price != '' && $max_price == '') {
      $price = " AND Price <= {$args['min-price']} ";
    }elseif($min_price == '' && $max_price != '') {
      $price = " AND Price >= {$args['max-price']} ";
    }

    $mls = '';
    if(isset($args['mls']) && trim($args['mls']) != '' ){
      $mls = " AND MLS = '{$args['mls']}'";
    }

    $args['where'] = ' WHERE 1=1 ' . $location . $type
    . $bathrooms . $bedrooms . $price . $mls;
    $args = wp_parse_args( $args, $defaults );

    $str_query = 'SELECT * FROM '.$this->table.' '.$args['where'].' '.$args['limit'].'';
    //echo $str_query;
    $listing = $this->wpdb->get_results($str_query);

    //$total_num_rows = $this->wpdb->get_var("SELECT COUNT(*) FROM {$this->table} {$args['where']}");
    $total_num_rows = $this->countData($args['where']);
    //return $listing;
    return [
      'listing' => $listing,
      'num_rows' => count($listing),
      'total_num_rows' => $total_num_rows,
    ];
  }

  public function countData($str_where = '')
  {
    return $this->wpdb->get_var( "SELECT COUNT(*) FROM $this->table $str_where" );
  }
}
