<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
class JF_Controllers_ImportProperty extends JF_Base{
    protected static $instance = null;
    protected $menu_slug = null;
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        /*
         * @TODO :
         *
         * - Uncomment following lines if the admin class should only be available for super admins
         */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }
    
    public function jf_import()
    {
        $data['heading'] = 'Import Property';
        $data['action'] = 'admin.php?page=jf-import';
        $data['method'] = 'post_search_listings';
        
        JF_View::get_instance()->admin_partials('partials/import-property.php', $data);
    }
    
    public function post_search_listings()
    {
      $upload_dir = wp_upload_dir();
      
      $parse = new JF_GetListings;
      
      $location_value = '';
      if( isset($_POST['location_value']) ) {
        $location_value = $_POST['location_value'];
      }
      
      $location_key = '';
      if( isset($_POST['location_key']) ) {
        if( $_POST['location_key'] != 'coords' ) {
          $location_key = $_POST['location_key'];
        } else {
        }
      }
      
      $search_location = array(
        'uri_query' => array('location['.$location_key.']' => $location_value),
        'format' => 'xml'
      );

      $parse = $parse->listings($search_location);

      $xmlString = $parse->getBody();
      $xml = simplexml_load_string($xmlString);

      $dom = new DOMDocument;
      $dom->preserveWhiteSpace = FALSE;
      $dom->loadXML($parse->getBody());
      
      //Save XML as a file
      $dom->save($upload_dir['basedir'] . '/placesterapi-listings.xml');
      
      $data['arr_xml'] = $xml;
      $data['upload_dir'] = $upload_dir;
      $data['download_xml'] = $upload_dir['baseurl'].'/placesterapi-listings.xml';
      //dd($data['arr_xml']->total);
      JF_View::get_instance()->admin_partials('partials/download-listings.php', $data);
    }
            
    /**
     * Controller
     *
     * @param   $action     string | empty
     * @parem   $arg        array
     *                      optional, pass data for controller
     * @return mix
     * */
    public function controller($action = '', $arg = array()){
        $this->call_method($this, $action);
    }
    
    public function __construct(){
        $this->menu_slug = JF_Admin_ImportProperty::get_instance()->menu_slug();
    }
}
