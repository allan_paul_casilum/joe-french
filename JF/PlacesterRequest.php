<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
class JF_PlacesterRequest{
  
  private $url_api = 'http://api.placester.com/';
  private $api_version = 'v2';
  private $api_format = 'json';
  
  /**
   * instance of this class
   *
   * @since 3.12
   * @access protected
   * @var null
   * */
  protected static $instance = null;

    /**
     * use for magic setters and getter
     * we can use this when we instantiate the class
     * it holds the variable from __set
     *
     * @see function __get, function __set
     * @access protected
     * @var array
     * */
    protected $vars = array();

    /**
   * Return an instance of this class.
   *
   * @since     1.0.0
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() 
  {

    /*
     * @TODO :
     *
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }
  
  /**
   * make request
   * */
  public function makeRequest($url_config = array())
  {
    $api_version = $this->api_version;
    if( isset($url_config['version']) ){
      $api_version = $url_config['version'];
    }
    
    $end_point = '';
    if( isset($url_config['end_point']) ){
      $end_point = $url_config['end_point'];
    }
    
    $format = $this->api_format;
    if( isset($url_config['format']) ){
      $format = '.' . $url_config['format'];
    }
    
    $api_key = PLACESTER_API;
    if( isset($url_config['api_key']) ){
      $api_key = $url_config['api_key'];
    }
    
    $uri_query = '';
    if( isset($url_config['uri_query']) ){
      $uri_query = '&'.http_build_query($url_config['uri_query']);
    }
        
    $url = $this->url_api . $api_version . '/' . $end_point . $format . '?api_key='.$api_key . $uri_query;
    
    $method = 'GET';
    if( isset($url_config['method']) ){
      $method = $url_config['method'];
    }

    $http_args = array();
    if( isset($url_config['http_args']) ){
      $http_args = $url_config['http_args'];
    }
    /*dd($url_config);
    dd($url);
    dd($http_args, 1);*/
    $http = new JF_Http;
    switch($method){
      case 'GET':
        return $http->httpGet($url,$http_args);
      break;
    }

  }
  
  public function __construct(){}
}
