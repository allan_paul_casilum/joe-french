<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
function jf_get_plugin_details(){
    // Check if get_plugins() function exists. This is required on the front end of the
    // site, since it is in a file that is normally only loaded in the admin.
    if ( ! function_exists( 'get_plugins' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $ret = get_plugins();
    return $ret['joe-french/joe-french.php'];
}
function jf_get_text_domain(){
    $ret = jf_get_plugin_details();
    return $ret['TextDomain'];
}
function jf_get_plugin_dir(){
    return plugin_dir_path( __FILE__ );
}
function jf_dd($array, $die = false){
  echo '<pre>';
    print_r($array);
  echo '</pre>';
  if($die){
    wp_die();
  }
}

function jf_get_listings_wprealty($args = []) {

  $limit = isset($args['limit_num']) ? $args['limit_num'] : 10;
  $paged = $args['paged'];
  $offset = ($paged * $limit);
  //echo $offset;
  /**
   * Define the array of defaults
   */
  $defaults = array(
  	'limit' => ' LIMIT '.$offset.', '.$limit.' ',
  );
  //jf_dd($args);
  /**
   * Parse incoming $args into an array and merge it with $defaults
   */
  $args = wp_parse_args( $args, $defaults );
  //print_r($args);
  $ret = JF_WpRealtyDB::get_instance()->getListingsDB($args);
  //jf_dd($ret);
  return $ret;
  //return $ret['listing'];
}

function jf_get_single_by_mls($mls) {
  $arr = [
    'limit' => ' LIMIT 1 ',
    'mls' => $mls,
  ];
  return JF_WpRealtyDB::get_instance()->getListingsDB($arr);
}

function jf_get_single_property_mls(){
  $slug = JF_CustomQueryVar::get_instance()->getSlug();
  $parse = get_query_var($slug);
  $property = [];
  $property_listings = [];
  if($parse){
  	$parse_arr = explode('-', $parse);
    $mls = end($parse_arr);
  	if($mls){
  	  $property = jf_get_single_by_mls($mls);
  	}
  }
  $title = '';
  if($property){
  	$property_listings = isset($property['listing'][0]) ? $property['listing'][0] : [];
  	$title = isset($property_listings->Address) ? $property_listings->Address : '';
  }
  return $property_listings;
}
