<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
class JF_Placester{
  /**
   * instance of this class
   *
   * @since 3.12
   * @access protected
   * @var null
   * */
  protected static $instance = null;

    /**
     * use for magic setters and getter
     * we can use this when we instantiate the class
     * it holds the variable from __set
     *
     * @see function __get, function __set
     * @access protected
     * @var array
     * */
    protected $vars = array();

    /**
   * Return an instance of this class.
   *
   * @since     1.0.0
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() {

    /*
     * @TODO :
     *
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }
  
  public function whoami()
  {
    $arg = array(
      'end_point' => 'organizations/whoami',
    );
    
    return JF_PlacesterRequest::get_instance()->makeRequest($arg);
  }
  
  public function getListings($search_filter = array())
  {
    $default_search_fitler = array(
      'limit' => 10
    );
        
    $search_filter = array_merge($default_search_fitler, $search_filter);
    
    $arg = array(
      'end_point' => 'listings',
      'version' => 'v2.1',
      'uri_query' => $search_filter['uri_query'],
      'format' => 'xml'
    );
    
    return JF_PlacesterRequest::get_instance()->makeRequest($arg);
  }
  
  public function getLocations($search_filter = array())
  {
    $default_search_fitler = array(
      'agency_id' => 3090113
    );
    
    $search_filter = array_merge($default_search_fitler, $search_filter);
    
    $arg = array(
      'end_point' => 'listings/locations',
      'uri_query' => $search_filter
    );
    
    /*add_filter( 'http_request_timeout', function(){
      return 70;
    });*/
    
    return JF_PlacesterRequest::get_instance()->makeRequest($arg);
  }
  
  public function __construct(){}
}
