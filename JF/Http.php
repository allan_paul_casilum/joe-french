<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
class JF_Http{
  
  private $response;
  
  /**
   * instance of this class
   *
   * @since 3.12
   * @access protected
   * @var null
   * */
  protected static $instance = null;

  /**
   * use for magic setters and getter
   * we can use this when we instantiate the class
   * it holds the variable from __set
   *
   * @see function __get, function __set
   * @access protected
   * @var array
   * */
  protected $vars = array();
  
  /**
   * timeout ms
   * */
  CONST TIMEOUT_MS = 30;
  /**
   * Return an instance of this class.
   *
   * @since     1.0.0
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() {

    /*
     * @TODO :
     *
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }
  
  public function httpGet($url, $args = array())
  {
    $response = wp_remote_get($url, $args);
    $this->response = $response;
    return $this;
  }
  
  public function getHeader()
  {
    return wp_remote_retrieve_headers($this->response);
  }

  public function getResponseCode()
  {
    return wp_remote_retrieve_response_code($this->response);
  }
  
  public function getBody()
  {
    return wp_remote_retrieve_body($this->response);
  }
  
  public function jf_custom_curl_timeout($handle)
  {
    curl_setopt( $handle, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT_MS ); // 30 seconds. Too much for production, only for testing.
    curl_setopt( $handle, CURLOPT_TIMEOUT, self::TIMEOUT_MS ); // 30 seconds. Too much for production, only for testing.
  }
  
  public function jf_custom_http_request_timeout( $timeout_value )
  {
    return self::TIMEOUT_MS;
  }
  
  public function jf_custom_http_request_args( $r )
  {
    $r['timeout'] = self::TIMEOUT_MS;
    return $r;
  }
  
  public function __construct()
  {
    add_action('http_api_curl', array($this, 'jf_custom_curl_timeout'), 9999, 1);
    add_action('http_request_timeout', array($this, 'jf_custom_http_request_timeout'), 9999);
    add_action('http_request_args', array($this, 'jf_custom_http_request_args'), 9999, 1);
  }
}
