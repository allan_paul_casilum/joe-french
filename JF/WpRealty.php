<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}
class JF_WpRealty{

  private $response;

  /**
   * instance of this class
   *
   * @since 3.12
   * @access protected
   * @var null
   * */
  protected static $instance = null;

  /**
   * use for magic setters and getter
   * we can use this when we instantiate the class
   * it holds the variable from __set
   *
   * @see function __get, function __set
   * @access protected
   * @var array
   * */
  protected $vars = array();

  /**
   * Return an instance of this class.
   *
   * @since     1.0.0
   *
   * @return    object    A single instance of this class.
   */
  public static function get_instance() {

    /*
     * @TODO :
     *
     * - Uncomment following lines if the admin class should only be available for super admins
     */
    /* if( ! is_super_admin() ) {
      return;
    } */

    // If the single instance hasn't been set, set it now.
    if ( null == self::$instance ) {
      self::$instance = new self;
    }

    return self::$instance;
  }

  public function __construct()
  {
    add_shortcode( 'jf_get_listings_wprealty', array($this, 'shortcodeListings') );
  }

  public function shortcodeListings($atts)
  {
    $atts = shortcode_atts( array(
  		'limit' => ' LIMIT 10 ',
	  ), $atts, 'jf_get_listings_wprealty' );

    $ret = JF_WpRealtyDB::get_instance()->getListingsDB($atts);
    $data['listings'] = $ret;
    /*ob_start();
	  ?> <HTML> <here> ... <?php
	  return ob_get_clean();*/
    //print_r($data);
    $template = JF_View::get_instance()->public_part_partials('partials/listing-wprealty.php');
    JF_View::get_instance()->display($template, $data);
  }
}
