<?php

/**
 * Fired during plugin deactivation
 *
 * @link       apyc.com
 * @since      1.0.0
 *
 * @package    Joe_French
 * @subpackage Joe_French/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Joe_French
 * @subpackage Joe_French/includes
 * @author     apyc <allan.paul.casilum@gmail.com>
 */
class Joe_French_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
