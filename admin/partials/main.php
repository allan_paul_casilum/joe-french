<div id="jf-wrapper" class="newsletter-wrap wrap">
    <h1><?php echo $heading;?></h1>
    <div class="feature-section one-col">
        <p class="lead-description">Some description here</p>
    </div>
    <form name="filter-posts-form" method="post" action="<?php echo $action;?>">
        <p class="submit"><input type="submit" name="query" id="query" class="button button-primary" value="Search"></p>
    </form>
</div>
